const { Client } = require('whatsapp-web.js');
const client = new Client();
const qrcode = require('qrcode-terminal');
const nodemailer = require('nodemailer'); 

async function run() {
  client.on('qr', qr => {
    qrcode.generate(qr, { small: true });
  });

  client.on('ready', () => {
    console.log('¡Bien! WhatsApp conectado.');
  });

  await client.initialize();

  function cumprimentar(opcion) {
    const dataAtual = new Date();
    const hora = dataAtual.getHours();

    let saudacao;

    if (opcion === 1) {
      if (hora >= 6 && hora < 12) {
        saudacao = "Hola, buenos días!";
      } else if (hora >= 12 && hora < 17) {
        saudacao = "Hola, buenas tardes!";
      } else {
        saudacao = "Hola, buenas noches!";
      }
    } else if (opcion === 2) {
      saudacao = "¡Hola! Por favor, proporciona algún dato (cedula, id, número de registro).";
    }

    return saudacao;
  }

  const delay = ms => new Promise(res => setTimeout(res, ms));

  client.on('message', async msg => {
    if (
      msg.body.match(/(buenas noches|buenos dias|buenas tardes|hola|dia|informacion|Imagen|videos|audios|teste)/i) &&
      msg.from.endsWith('@c.us')
    ) {
      const chat = await msg.getChat();
      chat.sendStateTyping();
      await delay(3000);
      const saudacoes = cumprimentar(1);
      await client.sendMessage(msg.from, `${saudacoes} `);

      let toEmail = '';
      let subject = '';
      let text = '';

      const mailOptions = {
        from: 'seu-email@gmail.com',
        to: toEmail,
        subject: subject,
        text: text
      };

      transporter.sendMail(mailOptions, function (error, info) {
        if (error) {
          console.log(error);
        } else {
          console.log('E-mail enviado: ' + info.response);
          const successMessage = 'E-mail enviado satisfactoriamente! 📧';
          client.sendMessage(msg.from, successMessage);
        }
      });
    } else if (
      msg.body.match(/(opcion2|segunda opcion|dos)/i) &&
      msg.from.endsWith('@c.us')
    ) {
      const saudacoes = cumprimentar(2);
      await client.sendMessage(msg.from, saudacoes);
      await client.sendMessage(msg.from, "Por favor, proporciona el dato:");


      const respuesta = await waitForResponse();

      // Obtener información
      const informacionAsociada = obtenerInformacionAsociada(respuesta.body);

      // Enviar la información al usuario
      await client.sendMessage(msg.from, informacionAsociada);
    }
  });

  function waitForResponse() {
    return new Promise((resolve, reject) => {
      client.on('message', async msg => {
        if (msg.from.endsWith('@c.us')) {
          resolve(msg);
        }
      });
    });
  }
}

run().catch(err => console.error(err));
